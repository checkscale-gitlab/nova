# Nova
This is a simple AWS Terraform project to setup a virtual machine in the AWS Cloud known as an ``instance``.

[Getting started wih Terraform](https://learn.hashicorp.com/tutorials/terraform/aws-build?in=terraform/aws-get-started)

## What Does This Project Do?
This project will create an AWS environment using [Alpine Linux](https://alpinelinux.org/) and setup [mStream](https://www.mstream.io/) music streaming service.

## What Will Be Built In The AWS
The system were this project is installed and ran from will be referred to as the ``Build System``.
- Discover the public IP address of the ``Build System``
- Create a new SSH key pair in the local directory that is exclusively for administering the EC2
- Create Security Groups to allow incoming web connections from the public Internet
- Create a Security Group to allow incoming SSH connections from the ``Build System``
- Create an EC2 Instance of Alpine Linux
- SSH from the ``Build System`` to the Alpine EC2 and install ``mStream``

## How To Use This
A ``Makefile`` is provided to simplify most of the steps. After installing the needed software and creating an AWS Account with IAM, the project can be setup by running ``make apply``

When done with the environment it can be destroyed to save from being charged AWS fees by running ``make destroyall``

## Software Needed
In addition to standard Linux programs such as an SSH client and shell, these programs may need extra steps to install using a package manager like ``yum`` or ``apt``.
- git
- make
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- [jq](https://stedolan.github.io/jq/)
- [terraform](https://www.terraform.io/downloads.html)

This project also requires a directory with less then 8GB of music to upload.

# Before You Begin
## Create An AWS Account
Open an account with [AWS](https://portal.aws.amazon.com/billing/signup). This will require a credit card. This project will be using free tier resource but some charges could still apply. Read and become familiar with [AWS Free Teir](https://aws.amazon.com/free/). This account is the root account and has no ability to create AWS resources with Terraform. To do that it will require the creation of an IAM account that will have an AWS Access Key and administrator permissions.

> **_NOTE:_** The development of this project was done using AWS 12 Month Free Tier and had zero charges.

## Create An Identity and Access Management (IAM) Account
Create an [IAM account](https://console.aws.amazon.com/iam/home). This will be the admin account for Terraform to manage deployments. It requires that AWS Access ID & AWS Access Key be added to the local system that is used to deploy the AWS resources. 

It is recommended to choose a region that the account is not currently using to avoid name and service collision. Here is a guide on how to choose a region [Save yourself a lot of pain (and money) by choosing your AWS Region wisely](https://www.concurrencylabs.com/blog/choose-your-aws-region-wisely/)

Once the AWS Access Key has been generated, use the AWS CLI to add it to the local credentials file. It is possible to manually edit the files in ``~/.aws/config`` and ``~/.aws/credentials`` but small mistakes with spacing or formatting can cause it to fail. Instructions to add the credentials are on [Configuration and credential file settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html). Use the profile name ``default``. For this project there can only be one profile in the credentials. If pre-existing credentials exist, rename the ``~/.aws/`` folder until done.


Test the credentials using AWS CLI.
``aws iam get-user | jq``

If successful the output will be JSON that looks much like this.
```json
{
  "User": {
    "Path": "/",
    "UserName": "TheIamUser",
    "UserId": "SOMESTRING",
    "Arn": "arn:aws:iam::<OWNER ID>:user/TheIamUser",
    "CreateDate": "YYYY-MM-DDTHH:mm:ss+00:00",
    "PasswordLastUsed": "YYYY-MM-DDTHH:mm:ss+00:00"
  }
}
```

# Installing Nova
The Makefile will initialize the Terraform and kickoff the deployment.

```bash
git clone https://gitlab.com/SiliconTao-Systems/nova.git
cd nova
make apply
```

There will be a prompt to enter the path to a music folder with less then 10GB of music files.

When Terraform is done setting up the system, the pubic IP address is printed to the output. Open a web browser to that IP using HTTP on port 80.

The steaming service is ready for use. Charge people for access to it and let them hear the music.

To avoid being charged for the AWS services, destroy the system when done.
```bash
make destroyall
```
Everything that was build has now been destroyed. 

## Improvements For Long Term Use
These steps are not part of the project but are recommended if the server will continue long term service in the cloud.

1. Increase storage size. This project uses 10GB but a real media streaming service should have several gigabytes or even terabytes. Consider using an S3 for large storage and review other storage options.

2. [EIP (Elastic IP)](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html) will allow the instance to keep the same IP address after a reboot. Building the EIP in [Terraform](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) will allow the instance to keep the same IP even after being destroyed and rebuilt by Terraform. This project gets assigned a new IP after each reboot. 

3. Register a domain. Having a domain allows for an SOA (Start Of Authority) DNS (Domain Name Server). With this the EIP can be resolved using a website name for the registered domain. Much better then using the IP address.

4. Security. Consider locking out public access by requiring password login or even a Security Group to restrict IP addresses.

5. The service script to run mStream could be improved. [Alpine Init System service](https://wiki.alpinelinux.org/wiki/Alpine_Linux_Init_System).





